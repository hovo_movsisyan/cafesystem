﻿using Menu.Models;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SeeReservs : ContentPage
    {
        public ObservableCollection<Reservation> reservations;
        readonly ApiServices apiServices = new ApiServices();
        public SeeReservs()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            reservations = new ObservableCollection<Reservation>();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var result = await apiServices.GetReservations();

            foreach (var item in result)
            {
                if (reservPage.ItemsSource == null)
                {
                    if (item.Status == 0 && item.Date >= DateTime.Now)
                    {
                        reservations.Add(item);
                    }
                }
            }
            if (reservations.Count == 0)
            {
                await this.DisplayAlert("Heeey!", "You do not have any Orders for table Reservation", "OK");
                Application.Current.MainPage = new NavigationPage(new CafePage());
            }
            reservPage.ItemsSource = reservations;
            RunningIndicator.IsRunning = false;
        }
        private async void Reject_Clicked(object sender, EventArgs e)
        {
            var result = await this.DisplayAlert("Warning!", "Do You want to Reject this Order", "Yes", "No");
            if (result)
            {
                var reservation = sender as Button;
                Reservation reject = reservation.CommandParameter as Reservation;
                var response = await apiServices.GetOrderStatus(reject.Id);
                int status = int.Parse(response);
                if (status == 0)
                {
                    await apiServices.RejectionById(reject.Id);
                    await DisplayAlert("REJECTION!!!!!", "Order has been Rejected", "OK");
                    reservations.Remove(reject);
                }
                reservPage.ItemsSource = null;
                reservPage.ItemsSource = await apiServices.GetResevationsStatus();
            }
        }

        private async void Approve_Clicked(object sender, EventArgs e)
        {
            var result = await this.DisplayAlert("Warning!", "Do You want to Approve this Order", "Yes", "No");
            if (result)
            {
                var reservation = sender as Button;
                Reservation approve = reservation.CommandParameter as Reservation;
                var response = await apiServices.GetOrderStatus(approve.Id);
                int status = int.Parse(response);
                if (status == 0)
                {
                    await apiServices.AproveById(approve.Id);
                    await DisplayAlert("APPROVMENT!!!!!", "Order has been Approved", "OK");
                    reservations.Remove(approve);
                }
                reservPage.ItemsSource = null;
                reservPage.ItemsSource = await apiServices.GetResevationsStatus();

            }
        }

    }
}