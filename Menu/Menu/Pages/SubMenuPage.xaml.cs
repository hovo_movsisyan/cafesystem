﻿using Menu.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubMenuPage : ContentPage
    {
        public ObservableCollection<SubMenu> SubMenus;
        public SubMenuPage(Menus menu)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            SubMenus = new ObservableCollection<SubMenu>();
            foreach (var item in menu.SubMenus)
            {
                SubMenus.Add(item);
            }
            SubMenuList.ItemsSource = SubMenus;
        }
    }
}