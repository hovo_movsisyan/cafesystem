﻿using Menu.Models;
using Menu.Services;
using Plugin.Clipboard;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubMenuEntryPage : ContentPage
    {
        readonly ApiServices apiServices = new ApiServices();
        public SubMenuEntryPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            mainMenuPicker.Items.Clear();
            var result = await apiServices.GetMenu();
            foreach (var item in result)
            {
                if (mainMenuPicker.ItemsSource == null)
                {
                    mainMenuPicker.Items.Add((item.Id).ToString() + ". " + item.Name);
                    mainMenuPicker.SelectedIndex = 0;
                }
            }
        }

        private async void SubMenuInsertion_Tapped(object sender, EventArgs e)
        {
            var item = mainMenuPicker.Items[mainMenuPicker.SelectedIndex];
            var myNumbers = item.Where(x => char.IsDigit(x)).ToArray();
            var myNewString = new String(myNumbers);
            if(myNewString=="")
                await DisplayAlert("Sorry!!!", " Please complete the fields", "Ok");
            SubMenu subMenu = new SubMenu()
            {
                Name = subname.Text,
                Description = subdesc.Text,
                Price = subprice.Text,
                Image = subimage.Text,
                Menu_Id = int.Parse(myNewString)
            };
            if (subname.Text != null && subprice.Text != null)
            {
                bool response = await apiServices.AddSubMenu(subMenu);
                if (response != true)
                    await DisplayAlert("Warning!!!", "Something goes wrong with connection", "Ok");
                else
                {
                    await DisplayAlert("Sucsess!!!", "You`re added sucsesfuly...", "Ok");
                    await Navigation.PushAsync(new ShowSubMenuPage());
                }  
            }
            else
                await DisplayAlert("Sorry!!!", " Please complete the fields", "Ok");
        }

        private async void SubMenuImageUrlInsertion_Tapped(object sender, EventArgs e)
        {
            // Copy text
            CrossClipboard.Current.SetText(subimage.Text);
            // Paste text
            subimage.Text = await CrossClipboard.Current.GetTextAsync();
        }
    }
}