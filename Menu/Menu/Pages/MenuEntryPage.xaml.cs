﻿using Menu.Models;
using Menu.Services;
using Plugin.Clipboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuEntryPage : ContentPage
    {
        readonly ApiServices apiServices = new ApiServices();
        public MenuEntryPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        private async void MenuInsertion_Tapped(object sender, EventArgs e)
        {
            Menus menu = new Menus()
            {
                Name = menuName.Text,
                Image = menuImage.Text,
            };
            if (menuName.Text != null)
            {
                bool response = await apiServices.AddMenu(menu);
                if (response != true)
                    await DisplayAlert("Warning!!!", "Something goes wrong with connection", "Ok");
                else
                {
                    await DisplayAlert("Sucsess!!!", "You're added sucsessfuly...", "Ok");
                    await Navigation.PushAsync(new ShowMenuPage());
                }  
            }
            else
                await DisplayAlert("Sorry!!!", " Please complete the fields", "Ok");
        }
        private async void MenuImageUrlInsertion_Tapped(object sender, EventArgs e)
        {
            // Copy text
            CrossClipboard.Current.SetText(menuImage.Text);
            // Paste text
            menuImage.Text = await CrossClipboard.Current.GetTextAsync();
        }
    }
}