﻿using Menu.Models;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowSubMenuPage : ContentPage
    {
        readonly ApiServices apiServices = new ApiServices();
        public ObservableCollection<SubMenu> menus;
        public ShowSubMenuPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            menus = new ObservableCollection<SubMenu>();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var result = await apiServices.GetSubMenu();
            foreach (var item in result)
            {
                if (submenulist.ItemsSource == null)
                {
                    menus.Add(item);
                }
            }
            submenulist.ItemsSource = menus;
            RunningIndicator.IsRunning = false;
            await apiServices.GetResevationsStatus();
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            var result = await this.DisplayAlert("Warning!", "Do you really want to delete menu?", "Yes", "No");
            if (result)
            {
                var menu = sender as Button;
                SubMenu delete = menu.CommandParameter as SubMenu;
                await apiServices.DeleteSubMenu(delete.SubMenuId);
                submenulist.ItemsSource = null;
                submenulist.ItemsSource = await apiServices.GetSubMenu();
            }
        }
    }
}