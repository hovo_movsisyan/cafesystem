﻿using Menu.Models;
using Menu.Services;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReservePage : ContentPage
    {
        readonly ApiServices apiServices = new ApiServices();
        public ReservePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            string id = App.Current.Properties["UserId"].ToString();
            current.Text = id;
        }
        private async void BtnBookTable_Clicked(object sender, EventArgs e)
        {
            Reservation reservation = new Reservation()
            {
                Name = EntName.Text,
                Email = EntEmail.Text,
                Phone = EntPhone.Text,
                TotalPeople = EntTotalPeople.Text == null ? 0 : int.Parse(EntTotalPeople.Text),
                Date = DpBookingDate.Date,
                Time = TpBookingTime.Time.ToString(),
                UserId = int.Parse(current.Text)
            };
            if (EntName.Text != null && EntEmail.Text != null && EntPhone.Text != null && EntTotalPeople.Text != null)
            {
                bool response = await apiServices.ReserveTable(reservation);
                if (response != true)
                    await DisplayAlert("Warning!!!", "Something goes wrong", "Ok");
                else
                    await DisplayAlert("Sucsess!!!", "Your order has sent, we'll let you know as soon as we reacive your order...", "Ok");
            }
            else
                await DisplayAlert("Sorry!!!", " Please complete the fields", "Ok");

            await Navigation.PushAsync(new HomePage());
        }
    }
}