﻿using Menu.Models;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        readonly ApiServices apiServices = new ApiServices();
        public ProfilePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        protected async override void OnAppearing()
        {
            string id = App.Current.Properties["UserId"].ToString();
            int userid = int.Parse(id);
            Users response = await apiServices.GetUserById(userid);
            name.Text = response.Name;
            email.Text = response.Email;
        }

        private async void OrderHistory_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MyOrderHistoryPage());
        }
    }
}