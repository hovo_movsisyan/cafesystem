﻿using Menu.Models;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowMenuPage : ContentPage
    {
        readonly ApiServices apiServices = new ApiServices();
        public ObservableCollection<Menus> menus;
        public ShowMenuPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            menus = new ObservableCollection<Menus>();
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var result = await apiServices.GetMenu();
            foreach (var item in result)
            {
                if (menulist.ItemsSource == null)
                {
                    menus.Add(item);
                }
            }
            menulist.ItemsSource = menus;
            RunningIndicator.IsRunning = false;
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            var result = await this.DisplayAlert("Warning!", "Do you really want to delete menu?", "Yes", "No");
            if (result)
            {
                var menu = sender as Button;
                Menus delete = menu.CommandParameter as Menus;
                await apiServices.DeleteMenu(delete.Id);
                menulist.ItemsSource = null;
                menulist.ItemsSource = await apiServices.GetMenu();
            }
              
        }
    }
}