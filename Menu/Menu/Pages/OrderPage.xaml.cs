﻿using Menu.Models;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderPage : ContentPage
    {
        public ObservableCollection<Reservation> orders;
        readonly ApiServices apiServices = new ApiServices();
        public OrderPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            orders = new ObservableCollection<Reservation>();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            string id = App.Current.Properties["UserId"].ToString();
            int userid = int.Parse(id);
            var result = await apiServices.GetOrderById(userid);
            foreach (var item in result)
            {
                if (order.ItemsSource == null)
                {
                    if (item.Status == 0 && item.Date >= DateTime.Now)
                    {
                        orders.Add(item);
                    }
                }
            }
            if (orders.Count == 0)
            {
                await this.DisplayAlert("Heeey!", "Go and Reserve your Table", "OK");
                Application.Current.MainPage = new NavigationPage(new HomePage());
            }
            
            order.ItemsSource = orders;
            RunningIndicator.IsRunning = false;
        }
    }
}