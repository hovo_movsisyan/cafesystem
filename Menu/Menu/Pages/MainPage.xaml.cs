﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

       
        private void Cafe_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CafePage());
        }

        private void Home_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SIgnUpPage());
        }
    }
}