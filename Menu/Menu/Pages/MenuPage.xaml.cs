﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Menu.Models;
using Menu.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public ObservableCollection<Menus> menus;
        readonly ApiServices apiServices = new ApiServices();
        public MenuPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            menus = new ObservableCollection<Menus>();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var result=await apiServices.GetMenu();
            foreach (var item in result)
            {
                if (HomeMenu.ItemsSource == null) 
                {
                    menus.Add(item);
                }
            }
            HomeMenu.ItemsSource = menus;
            RunningIndicator.IsRunning = false;
        }
        private void HomeMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Menus selectedMenu)
            {
                Navigation.PushAsync(new SubMenuPage(selectedMenu));
            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}