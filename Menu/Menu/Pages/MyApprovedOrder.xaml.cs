﻿using Menu.Models;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Menu.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyApprovedOrder : ContentPage
    {
        public ObservableCollection<Reservation> orders;
        readonly ApiServices apiServices = new ApiServices();
        public MyApprovedOrder()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            orders = new ObservableCollection<Reservation>();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            string id = App.Current.Properties["UserId"].ToString();
            int userid = int.Parse(id);
            var result = await apiServices.GetOrderById(userid);
            foreach (var item in result)
            {
                if (approvedorder.ItemsSource == null)
                {
                    if (item.Status == 1)
                        orders.Add(item);
                }
            }
            approvedorder.ItemsSource = orders;
            RunningIndicator.IsRunning = false;
        }
    }
}
