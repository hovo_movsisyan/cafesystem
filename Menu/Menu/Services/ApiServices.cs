﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Menu.Models;
using System.Threading.Tasks;
using System.Diagnostics;
using Menu.VIewModel;
using System.Net.Http.Headers;
using Menu.Pages;
using static System.Net.Mime.MediaTypeNames;
using System.Linq;

namespace Menu.Services
{
    public class ApiServices
    {
        //git clone https://hovo_movsisyan@bitbucket.org/hovo_movsisyan/cafewebapi.git
        //use this API .  REQUIRED

        readonly HttpClient client = new HttpClient();
        string baseUrl = "http://192.168.10.181:80/CafeSystem/api/";
        public async Task<List<Menus>> GetMenu()
        {
            var response = await client.GetStringAsync($"{baseUrl}Menus/GetMenus");
            return JsonConvert.DeserializeObject<List<Menus>>(response);
        }
        public async Task<List<SubMenu>> GetSubMenu()
        {
            var response = await client.GetStringAsync($"{baseUrl}Menus/GetSubMenus");
            return JsonConvert.DeserializeObject<List<SubMenu>>(response);
        }

        public async Task<List<Reservation>> GetReservations()
        {
            var response = await client.GetStringAsync($"{baseUrl}Reservations/GetReservations");
            return JsonConvert.DeserializeObject<List<Reservation>>(response);
        }
        public async Task<List<Reservation>> GetResevationsStatus()
        {
            var response = await client.GetStringAsync($"{baseUrl}Reservations/GetResevationsStatus");
            return JsonConvert.DeserializeObject<List<Reservation>>(response);
        }

        public async Task<bool> DeleteMenu(int id)
        {
            var response = await client.DeleteAsync(string.Concat($"{baseUrl}Menus/DeleteMenu?id=", id));
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> DeleteSubMenu(int id)
        {
            var response = await client.DeleteAsync(string.Concat($"{baseUrl}Menus/DeleteSubMenu?id=", id));
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> DeleteReservation(int id)
        {
            var response = await client.DeleteAsync(string.Concat($"{baseUrl}Reservations/DeleteReservation?id=", id));
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> Register(string name, string email, string password)
        {
            var model = new Users
            {
                Name = name,
                Email = email,
                Password = password,
            };
            var json = JsonConvert.SerializeObject(model);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Users/Register", content);
            return response.IsSuccessStatusCode;
        }
        public async Task<string> Login(string login, string password)
        {
            var model = new Users
            {
                Email = login,
                Password = password
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Users/Login", content);
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        public async Task<Users> GetUserById(int id)
        {
            var model = new Users
            {
                UserId = id
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Users/GetUserById", content);
            string responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Users>(responseBody);
        }
        public async Task<List<Reservation>> GetOrderById(int id)
        {
            var model = new Reservation
            {
                UserId = id
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Reservations/GetOrderById", content);
            string responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Reservation>>(responseBody);
        }
        public async Task<bool> ReserveTable(Reservation reservation)
        {
            var json = JsonConvert.SerializeObject(reservation);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Reservations/AddReservation", content);
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> RejectionById(int id)
        {
            var model = new Reservation
            {
                Id = id
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Reservations/RejectionById", content);
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> AproveById(int id)
        {
            var model = new Reservation
            {
                Id = id
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Reservations/AproveById", content);
            return response.IsSuccessStatusCode;
        }
        public async Task<string> GetOrderStatus(int id)
        {
            var model = new Reservation
            {
                Id = id
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Reservations/GetOrderStatus", content);
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        public async Task<bool> AddMenu(Menus menu)
        {
            var json = JsonConvert.SerializeObject(menu);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Menus/AddMenu", content);
            return response.IsSuccessStatusCode;
        }
        public async Task<bool> AddSubMenu(SubMenu subMenu)
        {
            var json = JsonConvert.SerializeObject(subMenu);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{baseUrl}Menus/AddSubMenu", content);
            return response.IsSuccessStatusCode;
        }
    }
}
