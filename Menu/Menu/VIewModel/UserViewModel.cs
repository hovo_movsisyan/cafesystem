﻿using Menu.Pages;
using Menu.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Menu.VIewModel
{
    public class UserViewModel : INotifyPropertyChanged
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        readonly ApiServices apiServices = new ApiServices();

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;
            changed(this, new PropertyChangedEventArgs(name));
        }
        public ICommand RegisterCommand
        {
            get
            {
                return new Command(async () =>
                {
                    if (Password != ConfirmPassword)
                    {
                        await Application.Current.MainPage.DisplayAlert("Sorry", "Plesae Confirm Password", "Ok");
                        return;
                    }
                    else
                    {
                        if (Name != null & Email != null && Password != null)
                        {
                            IsBusy = true;
                            var isSucces = await apiServices.Register(Name, Email, Password);
                            if (isSucces)
                            {
                                await Application.Current.MainPage.DisplayAlert("Success", "You are Sign up successfuly", "Login");
                                Application.Current.MainPage = new NavigationPage(new LoginPage());
                            }
                            else
                            {
                                await Application.Current.MainPage.DisplayAlert("Sorry", "Try to Sign up again", "Ok");
                                Application.Current.MainPage = new NavigationPage(new SIgnUpPage());
                            }
                        }
                        else
                            await Application.Current.MainPage.DisplayAlert("Sorry!!!", " Please for Registration complete the fields", "Ok");
                    }

                });
            }
        }
    }
}
