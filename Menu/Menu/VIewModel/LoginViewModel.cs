﻿using Menu.Models;
using Menu.Pages;
using Menu.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Menu.VIewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        readonly ApiServices apiServices = new ApiServices();
        private void OnPropertyChanged(string name)
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;
            changed(this, new PropertyChangedEventArgs(name));
        }
        public ICommand LoginCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    var success = await apiServices.Login(Username, Password);
                    UserId = int.Parse(success);
                    App.Current.Properties["UserId"] = UserId;
                    await App.Current.SavePropertiesAsync();
                    if (int.Parse(success) == 0)
                    {
                        await Application.Current.MainPage.DisplayAlert("Sorry", "Try to Login again or Sign up", "Ok");
                        Application.Current.MainPage = new NavigationPage(new SIgnUpPage());
                    }
                    else if (int.Parse(success) == UserId)
                    {
                        await Application.Current.MainPage.DisplayAlert("Success", "You are Login successfuly", "Thanks");
                        Application.Current.MainPage = new NavigationPage(new HomePage());
                    }
                });
            }
        }

    }
}
